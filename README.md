# Stack

[![npm-version](https://img.shields.io/npm/v/@typemon/stack.svg)](https://www.npmjs.com/package/@typemon/stack)
[![npm-downloads](https://img.shields.io/npm/dt/@typemon/stack.svg)](https://www.npmjs.com/package/@typemon/stack)

push push push peek pop



# Usage

```
$ npm install @typemon/stack
```

```typescript
import { Stack } from '@typemon/stack';
```

```typescript
const stack: Stack<string> = new Stack(['a']); // ['a'] count = 1

stack.push('b');  // ['a', 'b']      count = 2
stack.push('c');  // ['a', 'b', 'c'] count = 3

stack.peek(); // 'c' ['a', 'b', 'c'] count = 3

stack.pop();  // 'c' ['a', 'b']      count = 2
```

### Get Item
```typescript
const stack: Stack<string> = new Stack(['a']);

stack.get(0);  // 'a'

stack.get(1);  // error: 'The index is out of range.'
stack.get(-1); // error: 'The index is out of range.'
```
