//
//
//
export class Stack<Item> implements Iterable<Item> {
    public static isStack(o: any): o is Stack<any> {
        return o instanceof Stack;
    }

    protected readonly items: Item[];

    constructor(items?: Iterable<Item>) {
        if (items === undefined) {
            this.items = [];
        }
        else if (Stack.isStack(items)) {
            this.items = items.items.slice();
        }
        else if (Array.isArray(items)) {
            this.items = items.slice();
        }
        else {
            this.items = [...items];
        }
    }

    public get count(): number {
        return this.items.length;
    }

    public [Symbol.iterator](): Iterator<Item> {
        let index: number = 0;

        return {
            next: () => {
                return {
                    done: index >= this.count,
                    value: this.items[index++]
                };
            }
        };
    }

    public get(index: number): Item {
        if (index < 0 || index >= this.count) {
            throw new Error('The index is out of range.');
        }

        return this.items[index];
    }

    public push(item: Item): void {
        this.items.push(item);
    }
    public pop(): Item {
        if (this.count === 0) {
            throw new Error('The stack is empty.');
        }

        return this.items.pop() as Item;
    }
    public peek(): Item {
        if (this.count === 0) {
            throw new Error('The stack is empty.');
        }

        return this.items[this.count - 1];
    }

    public clear(): void {
        this.items.splice(0, this.count);
    }

    public clone(): Stack<Item> {
        return new Stack(this);
    }

    public toArray(): Item[] {
        return this.items.slice();
    }
    public toString(): string {
        return this.items.toString();
    }
}
